module.exports = function(grunt) {
    grunt.initConfig({
        bowercopy: {
            options: {
                srcPrefix: 'bower_components'
            },

            libs: {
                options: {
                    destPrefix: 'public/libs'
                },

                files: {
                    // Angular JS
                    'angular.min.js': 'angular/angular.min.js',
                    'angular.min.js.map': 'angular/angular.min.js.map',

                    // Angular Route
                    'angular-route.min.js': 'angular-route/angular-route.min.js',
                    'angular-route.min.js.map': 'angular-route/angular-route.min.js.map',

                    // Angular Resource
                    'angular-resource.min.js': 'angular-resource/angular-resource.min.js',
                    'angular-resource.min.js.map': 'angular-resource/angular-resource.min.js.map'
                }
            }
        },

        zip_directories: {
            myzip: {
                files: [{
                    filter: 'isDirectory',
                    expand: true,
                    cwd: './',
                    src: ['public'],
                    dest: './target'
                }]
            }
        }

        //replace: {
        //    dist: {
        //        options: {
        //            patterns: [
        //                {
        //                    match: /\.\/images\//g,
        //                    replacement: '../images/'
        //                }
        //            ]
        //        },
        //        files: [
        //            {expand: true, flatten: true, src: ['static/vendor/css/trumbowyg.min.css'], dest: 'static/vendor/css/'}
        //        ]
        //    }
        //},
    });

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks('grunt-zip-directories');

    grunt.registerTask('run', [
        'bowercopy'
    ]);

    grunt.registerTask('zip', [
        'zip_directories'
    ]);
};