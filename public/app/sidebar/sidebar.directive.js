angular.module('app').directive('sidebar', function() {
    return {
        templateUrl: function(elem, attr){
            return '../app/sidebar/sidebar.'+attr.type+'.html';
        }
    };
});