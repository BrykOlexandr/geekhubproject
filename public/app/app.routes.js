(function () {
    "use strict";

    angular
        .module('app')
        .config(config);

    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider
            .when('/', {
                templateUrl: '../app/product/product.html',
                controller: 'ProductController',
                controllerAs: 'pr'
            })
            .when('/user', {
                templateUrl: '../app/user/user.html',
                controller: 'UserController',
                controllerAs: 'us'
            })
            .when('/user/albums', {
                templateUrl: '../app/user/albums.html',
                controller: 'AlbumController',
                controllerAs: 'us'
            })
            .otherwise({
                redirectTo: '/'
            })
    }
})();