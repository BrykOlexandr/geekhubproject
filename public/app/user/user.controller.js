angular.module('app').controller('UserController', [
    '$scope',
    'UserFactory',
    //'PhotoFactory',
    function ($scope, User) {
        refreshList();

        $scope.saveU = function () {
            User.save($scope.user, function() {
                refreshList();
                $scope.user = {}
            });
        };

        //$scope.saveP = function () {
        //    Photo.save($scope.photo, function() {
        //        refreshList();
        //        $scope.photo = {}
        //    });
        //};

        function refreshList() {
            $scope.users = User.query();
            //$scope.photos = Photo.query();
        }
    }
]);