angular.module('app').controller('ProductController', [
    '$scope',
    'ProductFactory',
    function ($scope, Product) {
        refreshList();

        $scope.hello = 'HELLO!!!';

        $scope.save = function () {
            Product.save($scope.product, function() {
                refreshList();
                $scope.product = {}
            });
        };

        function refreshList() {
            $scope.products = Product.query();
        }
    }
]);