angular.module('app', ['ngResource', 'ngRoute']);

angular.module('app').factory('ProductFactory', [
    '$resource',
    function ($resource) {
        return $resource('/product/');
    }
]);

angular.module('app').factory('UserFactory', [
    '$resource',
    function ($resource) {
        return $resource('/user/');
    }
]);

angular.module('app').factory('PhotoFactory', [
    '$resource',
    function ($resource) {
        return $resource('/photo/');
    }
]);