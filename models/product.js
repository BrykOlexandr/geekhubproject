var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    name: { type: String, required: true },
    description: String,
    price: Number
});

schema.set('autoIndex', false);
module.exports = mongoose.model('Product', schema, 'Product');