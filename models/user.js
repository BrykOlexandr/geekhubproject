var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true }
});

schema.set('autoIndex', false);
module.exports = mongoose.model('User', schema, 'User');