var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var schema = new Schema({
    title: String,
    name: { type: String, required: true },
    album: { type: String, required: true },
    user: { type: Schema.ObjectId, ref: 'User' },
    description: String,
    created: { type: Date, default: Date.now },
    edited: Date,
    comments: [{ body: String, date: Date }],
    views: { type: Number, default: 0 }
});

schema.set('autoIndex', false);
module.exports = mongoose.model('Photo', schema, 'Photos');