var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Express',
    product: [
      {
        "sku": 43900,
        "name": "Valera",
        "regularPrice": 5.49,
        "salePrice": 5.49
      },
      {
        "sku": 7150065,
        "name": "Dynex™ - AAA Batteries (48-Pack)",
        "regularPrice": 11.49,
        "salePrice": 11.49
      },
      {
        "sku": 2088495,
        "name": "Energizer - MAX AAA Batteries (8-Pack) - Silver",
        "regularPrice": 6.99,
        "salePrice": 6.99
      }
    ]
  });
});

module.exports = router;
