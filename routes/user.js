var express = require('express'),
    User = require('../models/user'),
    router = express.Router();

router.get('/', function(req, res, next) {
  User.find({reportName: req.reportName}, function (err, user) {
    if(err) {
      return next(err)
    }

    res.send(user);
  });
});

router.post('/', function(req, res, next) {
  User.create(req.body, function (err, user) {
    if(err) {
      return next(err)
    }
    res.send(user);
  });
});

module.exports = router;