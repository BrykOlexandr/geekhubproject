var express = require('express'),
    //fs = require('fs-extra'),
    Product = require('../models/product'),
    router = express.Router();

router.get('/', function(req, res, next) {
    Product.find({reportName: req.reportName}, function (err, product) {
        if(err) {
            return next(err)
        }

        res.send(product);
    });
});

router.post('/', function(req, res, next) {
    Product.create(req.body, function (err, product) {
        if(err) {
            return next(err)
        }
        res.send(product);
    });
});

module.exports = router;